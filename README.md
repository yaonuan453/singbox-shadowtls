# Sing-box + ShadowTLS 一键管理脚本

```shell
wget -N --no-check-certificate https://gitlab.com/misakablog/singbox-shadowtls/-/raw/main//sing-box.sh && bash sing-box.sh
```

## 感谢

项目原作：https://github.com/SagerNet/sing-box

YouTube 不良林：https://www.youtube.com/watch?v=o-IFEu4GENE
